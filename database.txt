1.buat Database
CREATE DATABASE myshop;

 

2. CREATE TABLE
CREATE TABLE users(
id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

CREATE TABLE categories(
id int AUTO_INCREMENT primary KEY,
    name varchar(255)
);

CREATE TABLE ITEMS(
id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    description varchar(255),
    price INT,
    stock INT,
    category_id INT,
    FOREIGN KEY(category_id) REFERENCES categories(id)
)

3. insert
USERS
insert into users (name, email, password)
VALUES ("John Doe", "joh@doe.com", "joh123"),
("Jane Doe","jane@doe.com","jenita123")

CATEGORIES
INSERT INTO categories (name)
VALUES("gadget"),("cloth"),("men"),("women"),("branded")

ITEMS
INSERT INTO items(name, description, price, stock, category_id)
VALUES("Sumsang b50","hape keren dari merek sumsang", 4000000, 100, 1),
("Uniklooh","baju keren dari brand ternama", 	500000, 50,2),
("IMHO Watch","Jam tangan anak yang jujur banget", 2000000, 10,1)



4. mengambil data dari database

a mengambil data user
SELECT name, email
FROM users

b.bmengambil data items

SELECT *
FROM items
WHERE price > 1000000

SELECT *
FROM items
WHERE name LIKE '%uniklo%' OR name like '%watch%' OR name LIKE '%sang%'


c.menampilkan data items join dengan kategori

SELECT i.name, i.description, i.price, i.stock, c.id, c.name as category
FROM categories c JOIN items i on c.id = i.id

5. mengubah data dari database

UPDATE items
set price = 2500000
where name = 'sumsang b50'